create database corendon;
use corendon;

CREATE TABLE IF NOT EXISTS foundLuggage(
lostAndFoundID VARCHAR(10) NOT NULL PRIMARY KEY, flightNumber VARCHAR(45),
timefound VARCHAR(45),  airportDeparture VARCHAR(45),
 dateFound DATETIME(6), airportArrival VARCHAR(45),
passengerName VARCHAR (45), passengerCity VARCHAR (45),
 labelNumber VARCHAR(45), typeLuggage VARCHAR (45),
brandLuggage VARCHAR (45), color VARCHAR (45), specialCharacteristics VARCHAR (400)
);


CREATE TABLE IF NOT EXISTS airportlist ( 
iatacode varchar(4) NOT NULL PRIMARY KEY,  
airportname varchar(45) NOT NULL );

INSERT INTO airportlist (iatacode, airportname)
VALUES ('AMS', 'Amsterdam'),
('AYT', 'Antalya'),
('IST', 'Istanbul'),
('BJV' , 'Bodrum'),
('DLM' , 'Dalaman'),
('ADB' , 'Izmir'),
('GZP' , 'Gazipasa-Alanaya'),
('ECN' , 'Nicosia-Ercan'),
('RAK' , 'Marakkech'),
('HER' , 'Crete(Heraklion)'),
('KGS' , 'Kos'),
('RHO' , 'Rodes'),
('ZTH' , 'Zakynthos'),
('CFU' , 'Corfu'),
('MJT' , 'Myteline'),
('OHD' , 'Ohrid'),
('SMI' , 'Samos'),
('LPA' , 'Gran Canaria'),
('TFO' , 'Tenerife'),
('PMI' , 'Palma de Mallorca'),
('AGP' , 'Mallaga'),
('FUE' , 'Fuerteventura'),
('FAO' , 'Faro'),
('ACE' , 'Lanzarote'),
('HRG' , 'Hurghada'),
('NBE' , 'Enfidha'),
('DXB' , ' Dubai'),
('BOJ' , 'Burgas '),
('BJL' , 'Banjul '),
('CTA' , 'Sicily (Catania)');


