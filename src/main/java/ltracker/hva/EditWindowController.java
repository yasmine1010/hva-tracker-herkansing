/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ltracker.hva;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ltracker.hva.classes.FoundLuggage;
import ltracker.hva.classes.TableViewData;
import ltracker.hva.database.MyJDBC;
import static ltracker.hva.mainSceneController.selectedID;

/**
 * FXML Controller class
 *
 * @author Admin
 */
public class EditWindowController implements Initializable {

    @FXML
    private Button closeButton;
    @FXML private Button saveButton;
    @FXML
    private TextField lostAndFoundID;
    @FXML
    private TextField flightNumber;
    @FXML
    private TextField timeFound;
    @FXML
    private TextField airportArrival;
    @FXML
    private DatePicker dateFound;
    @FXML
    private TextField airportDeparture;
    @FXML
    private TextField passengerName;
    @FXML
    private TextField passengerCity;
    @FXML
    private TextField labelNumber;
    @FXML
    private TextField typeLuggage;
    @FXML
    private TextField brandLuggage;
    @FXML
    private TextField color;
    @FXML
    private TextField specialCharacteristics;

    /**
     * Initializes the controller class.
     */
    @FXML
    private FoundLuggage luggage = new FoundLuggage();

    @FXML
    private void saveButtonAction() throws SQLException {

        MyJDBC myJDBC = new MyJDBC("corendon");
        MyJDBC myjdbc = new MyJDBC("corendon");

     myJDBC.executeUpdateQuery("DELETE FROM foundLuggage\n"
                + "WHERE lostAndFoundID =" + selectedID);
        // todo, copy schermvelden naar luggage
        luggage.setLostAndFoundID(lostAndFoundID.getText());
        luggage.setFlightNumber(flightNumber.getText());
        luggage.setTimeFound(timeFound.getText());
        luggage.setAirportDeparture(airportDeparture.getText());
        luggage.setDateFound(dateFound.getValue());
        luggage.setAirportArrival(airportArrival.getText());
        luggage.setPassengerName(passengerName.getText());
        luggage.setPassengerCity(passengerCity.getText());
        luggage.setLabelNumber(labelNumber.getText());
        luggage.setTypeLuggage(typeLuggage.getText());
        luggage.setBrandLuggage(brandLuggage.getText());
        luggage.setColor(color.getText());
        luggage.setSpecialCharacteristics(specialCharacteristics.getText());
        luggage.insertFoundLuggage(myjdbc);

//        Stage stage = (Stage) closeButton.getScene().getWindow();
//        stage.close();

      Stage stage = (Stage) saveButton.getScene().getWindow();
        // do what you have to do
        stage.close();


    }

    @FXML
    private void cancelButtonAction() {

        // get a handle to the stage
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // do what you have to do
        stage.close();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            MyJDBC myJDBC = new MyJDBC("corendon");

            ResultSet rs = myJDBC.executeResultSetQuery(
                    "SELECT * FROM corendon.foundluggage WHERE lostAndFoundID =" + selectedID);

            while (rs.next()) {
                // add each item from the ResultSet to the ComboBox as a string
                System.out.println(rs.getString(2));
                System.out.println(rs.getString("lostAndFoundID"));

                lostAndFoundID.setText(rs.getString("lostAndFoundID"));
                flightNumber.setText(rs.getString("flightNumber"));
                timeFound.setText(rs.getString("airportDeparture"));
                airportDeparture.setText(rs.getString("airportDeparture"));
                dateFound.setValue(rs.getDate("dateFound").toLocalDate());
                airportArrival.setText(rs.getString("airportArrival"));
                passengerName.setText(rs.getString("passengerName"));
                passengerCity.setText(rs.getString("passengerCity"));
                labelNumber.setText(rs.getString("labelNumber"));
                typeLuggage.setText(rs.getString("typeLuggage"));
                brandLuggage.setText(rs.getString("brandLuggage"));
                color.setText(rs.getString("color"));
                specialCharacteristics.setText(rs.getString("specialCharacteristics"));
                   // get a handle to the stage
     

            }
         

        } catch (SQLException ex) {
            Logger.getLogger(EditWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
     
    }

    public Button getCloseButton() {
        return closeButton;
    }

    public void setCloseButton(Button closeButton) {
        this.closeButton = closeButton;
    }

    public TextField getLostAndFoundID() {
        return lostAndFoundID;
    }

    public void setLostAndFoundID(TextField lostAndFoundID) {
        this.lostAndFoundID = lostAndFoundID;
    }

    public TextField getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(TextField flightNumber) {
        this.flightNumber = flightNumber;
    }

    public TextField getTimeFound() {
        return timeFound;
    }

    public void setTimeFound(TextField timeFound) {
        this.timeFound = timeFound;
    }

    public TextField getAirportArrival() {
        return airportArrival;
    }

    public void setAirportArrival(TextField airportArrival) {
        this.airportArrival = airportArrival;
    }

    public DatePicker getDateFound() {
        return dateFound;
    }

    public void setDateFound(DatePicker dateFound) {
        this.dateFound = dateFound;
    }

    public TextField getAirportDeparture() {
        return airportDeparture;
    }

    public void setAirportDeparture(TextField airportDeparture) {
        this.airportDeparture = airportDeparture;
    }

    public TextField getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(TextField passengerName) {
        this.passengerName = passengerName;
    }

    public TextField getPassengerCity() {
        return passengerCity;
    }

    public void setPassengerCity(TextField passengerCity) {
        this.passengerCity = passengerCity;
    }

    public TextField getLabelNumber() {
        return labelNumber;
    }

    public void setLabelNumber(TextField labelNumber) {
        this.labelNumber = labelNumber;
    }

    public TextField getTypeLuggage() {
        return typeLuggage;
    }

    public void setTypeLuggage(TextField typeLuggage) {
        this.typeLuggage = typeLuggage;
    }

    public TextField getBrandLuggage() {
        return brandLuggage;
    }

    public void setBrandLuggage(TextField brandLuggage) {
        this.brandLuggage = brandLuggage;
    }

    public TextField getColor() {
        return color;
    }

    public void setColor(TextField color) {
        this.color = color;
    }

    public TextField getSpecialCharacteristics() {
        return specialCharacteristics;
    }

    public void setSpecialCharacteristics(TextField specialCharacteristics) {
        this.specialCharacteristics = specialCharacteristics;
    }

}
