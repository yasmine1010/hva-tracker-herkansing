/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ltracker.hva;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;
import ltracker.hva.classes.TableViewData;
import ltracker.hva.database.MyJDBC;

/**
 * FXML Controller class
 *
 * @author Admin
 */
public class StatisticsController implements Initializable {

    @FXML
    Button registerScreenStatisticsButton;
    @FXML
    PieChart pieChart;

    
    private void fillPieChart() throws SQLException {
        MyJDBC myJDBC = new MyJDBC("corendon");
        ObservableList<PieChart.Data> piechartdata = FXCollections.observableArrayList();
        ResultSet rs = myJDBC.executeResultSetQuery(
                //NOTE: without the "\n" this query doesn't work do not remove it.
                "SELECT airportDeparture, count(airportDeparture) as aantal  FROM corendon.foundluggage\n"
                + "GROUP BY airportdeparture;");
        while (rs.next()) {
            piechartdata.add(new PieChart.Data(rs.getString("airportDeparture"), rs.getInt("aantal")));
//            piechartdata.add(new PieChart.Data("asda", 1));
            pieChart.setData(piechartdata);
        }

    }

    @FXML
    private void registerButtonStatisticsPushed(ActionEvent event) throws IOException {
        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("registerLuggage.fxml"));
        Scene registLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registLuggageScene);
        window.show();

    }

    @FXML
    private void searchButtonStatisticsPushed(ActionEvent event) throws IOException {
        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("mainScene.fxml"));
        Scene registLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registLuggageScene);
        window.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            fillPieChart();
        } catch (SQLException ex) {
            Logger.getLogger(StatisticsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
