/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ltracker.hva.classes;

import ltracker.hva.database.MyJDBC;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import ltracker.hva.mainSceneController;

/**
 *
 * @author Admin
 */
public class FoundLuggage {

    private String lostAndFoundID;
    private String flightNumber;
    private String timeFound;
    private String airportDeparture;
    private LocalDate dateFound;
    private String airportArrival;
    private String passengerName;
    private String passengerCity;
    private String labelNumber;
    private String typeLuggage;
    private String brandLuggage;
    private String color;
    private String specialCharacteristics;

    private void updateFoundLuggage() {

    }

    public void insertFoundLuggage(MyJDBC myJDBC) {

        myJDBC.executeUpdateQuery("INSERT INTO foundLuggage VALUES ("
                + "'" + this.lostAndFoundID + "',"
                + "'" + this.flightNumber + "',"
                + "'" + this.timeFound + "',"
                + "'" + this.airportDeparture + "',"
                + "'" + this.dateFound + "',"
                + "'" + this.airportArrival + "',"
                + "'" + this.passengerName + "',"
                + "'" + this.passengerCity + "',"
                + "'" + this.labelNumber + "',"
                + "'" + this.typeLuggage + "',"
                + "'" + this.brandLuggage + "',"
                + "'" + this.color + "',"
                + "'" + this.specialCharacteristics + "')");

    }

    public void getAirportList(ComboBox airportArrival) throws IOException, SQLException {
        MyJDBC myJDBC = new MyJDBC("corendon");

        ResultSet rs = myJDBC.executeResultSetQuery(
                "SELECT airportname FROM AirPortlist");
        while (rs.next()) {
            // add each item from the ResultSet to the ComboBox as a string
            airportArrival.getItems().add(rs.getString("airportname"));

        }
        System.out.println("Added values to the combobox");
        // print all items in the combobox "airportArrival"
        System.out.println(airportArrival.getItems());
        rs.close();
        myJDBC.close();

        // close the connection with the database
    }

    public String getLostAndFoundID() {
        return lostAndFoundID;
    }

    public void setLostAndFoundID(String lostAndFoundID) {
        this.lostAndFoundID = lostAndFoundID;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getTimeFound() {
        return timeFound;
    }

    public void setTimeFound(String timeFound) {
        this.timeFound = timeFound;
    }

    public String getAirportDeparture() {
        return airportDeparture;
    }

    public void setAirportDeparture(String airportDestination) {
        this.airportDeparture = airportDestination;
    }

    public LocalDate getDateFound() {
        return dateFound;
    }

    public void setDateFound(LocalDate dateFound) {
        this.dateFound = dateFound;
    }

    public String getAirportArrival() {
        return airportArrival;
    }

    public void setAirportArrival(String airportArrival) {
        this.airportArrival = airportArrival;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerCity() {
        return passengerCity;
    }

    public void setPassengerCity(String passengerCity) {
        this.passengerCity = passengerCity;
    }

    public String getLabelNumber() {
        return labelNumber;
    }

    public void setLabelNumber(String labelNumber) {
        this.labelNumber = labelNumber;
    }

    public String getTypeLuggage() {
        return typeLuggage;
    }

    public void setTypeLuggage(String typeLuggage) {
        this.typeLuggage = typeLuggage;
    }

    public String getBrandLuggage() {
        return brandLuggage;
    }

    public void setBrandLuggage(String brandLuggage) {
        this.brandLuggage = brandLuggage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSpecialCharacteristics() {
        return specialCharacteristics;
    }

    public void setSpecialCharacteristics(String specialCharacteristics) {
        this.specialCharacteristics = specialCharacteristics;
    }

}
