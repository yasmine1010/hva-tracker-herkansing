/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ltracker.hva.classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ltracker.hva.database.MyJDBC;

/**
 *
 * @author Admin
 */
public class TableViewData {

    private String ID;
    private String date;
    private String time;
    private String name;
    private String passengerCity;
    private String airportDeparture;
    private String flightNumber;
    private String type;
    private String brand;
    private String color;

    public TableViewData(String ID, String date, String time, String name, String passengerCity, String airportDeparture, String flightNumber, String type, String brand, String color) {
        this.ID = ID;
        this.date = date;
        this.time = time;
        this.name = name;
        this.passengerCity = passengerCity;
        this.airportDeparture = airportDeparture;
        this.flightNumber = flightNumber;
        this.type = type;
        this.brand = brand;
        this.color = color;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassengerCity() {
        return passengerCity;
    }

    public void setPassengerCity(String passengerCity) {
        this.passengerCity = passengerCity;
    }

    public String getAirportDeparture() {
        return airportDeparture;
    }

    public void setAirportDeparture(String airportDeparture) {
        this.airportDeparture = airportDeparture;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

  
}
