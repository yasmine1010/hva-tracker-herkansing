package ltracker.hva;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import ltracker.hva.database.MyJDBC;
import ltracker.hva.classes.FoundLuggage;
import ltracker.hva.classes.TableViewData;

public class mainSceneController implements Initializable {
    
    public static String selectedID;
    //Tableview
    @FXML
    private TableView<TableViewData> table;
    @FXML
    private TableColumn<TableViewData, String> ID;
    @FXML
    private TableColumn<TableViewData, String> date;
    @FXML
    private TableColumn<TableViewData, String> time;
    @FXML
    private TableColumn<TableViewData, String> name;
    @FXML
    private TableColumn<TableViewData, String> city;
    @FXML
    private TableColumn<TableViewData, String> airportDeparture;
    @FXML
    private TableColumn<TableViewData, String> flightNumber;
    @FXML
    private TableColumn<TableViewData, String> type;
    @FXML
    private TableColumn<TableViewData, String> brand;
    @FXML
    private TableColumn<TableViewData, String> color;
    @FXML
    private ObservableList<TableViewData> luggageData;

    @FXML
    private TextField filterField;
    //When register button is pushed switch scenes

    @FXML
    private void registerButtonPushed(ActionEvent event) throws IOException, SQLException {
        FoundLuggage luggage = new FoundLuggage();

        Parent registerLuggageParent = FXMLLoader.load(getClass().getResource("/ltracker/hva/registerLuggage.fxml"));
        Scene registerLuggageScene = new Scene(registerLuggageParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(registerLuggageScene);
        window.show();

    }

    @FXML
    public void editButtonAction(ActionEvent event) throws IOException {

        selectedID = table.getSelectionModel().getSelectedItem().getID();
        
        FXMLLoader naam = new FXMLLoader(getClass().getResource("editWindow.fxml"));
        Parent root1 = (Parent) naam.load();
        Stage stage = new Stage();
        stage.setTitle("edit Luggage");
        stage.setScene(new Scene(root1));
        stage.show();

    }

    @FXML
    private void deleteButtonPushed() throws IOException {
        MyJDBC myJDBC = new MyJDBC("corendon");
        System.out.println(table.getSelectionModel().getSelectedItem().getID());
        myJDBC.executeUpdateQuery("DELETE FROM foundLuggage\n"
                + "WHERE lostAndFoundID =" + table.getSelectionModel().getSelectedItem().getID());
        luggageData.clear();
        {
        ID.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("ID"));
        date.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("date"));
        time.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("time"));
        name.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("name"));
        city.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("passengerCity"));
        airportDeparture.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("airportDeparture"));
        flightNumber.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("flightNumber"));
        type.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("type"));
        brand.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("brand"));
        color.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("color"));

        try {
            table.setItems(getLuggageData());

        } catch (SQLException ex) {
            Logger.getLogger(mainSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
      
    }
    }

    @FXML
    private void statisticsButtonPushed(ActionEvent event) throws IOException {

        Parent statisticsParent = FXMLLoader.load(getClass().getResource("/ltracker/hva/statistics.fxml"));
        Scene statisticsScene = new Scene(statisticsParent);

        //this line gets the stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(statisticsScene);
        window.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {
        ID.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("ID"));
        date.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("date"));
        time.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("time"));
        name.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("name"));
        city.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("passengerCity"));
        airportDeparture.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("airportDeparture"));
        flightNumber.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("flightNumber"));
        type.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("type"));
        brand.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("brand"));
        color.setCellValueFactory(new PropertyValueFactory<TableViewData, String>("color"));

        try {
            table.setItems(getLuggageData());

        } catch (SQLException ex) {
            Logger.getLogger(mainSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }

        FilteredList<TableViewData> filteredData = new FilteredList<>(luggageData, p -> true);

        // 2. Set the filter Predicate whenever the filter changes.
        filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(person -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (person.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (person.getAirportDeparture().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.  
                } else if (person.getBrand().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.  
                } else if (person.getColor().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.  
                }
                return false; // Does not match.
            });
        });
        //filter contains: name, airportDeparture, brand, color
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<TableViewData> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(table.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        table.setItems(sortedData);

    }

    public ObservableList<TableViewData> getLuggageData() throws SQLException {
        MyJDBC myJDBC = new MyJDBC("corendon");
        luggageData = FXCollections.observableArrayList();
        ResultSet rs = myJDBC.executeResultSetQuery(
                "SELECT lostAndFoundID, dateFound, timeFound, passengerName, passengerCity, airportDeparture, flightNumber, typeLuggage, brandLuggage, color FROM foundLuggage");
        while (rs.next()) {
            // add each item from the ResultSet to the ComboBox as a string
            luggageData.add(new TableViewData(rs.getString("lostAndFoundID"), rs.getDate("dateFound").toString(), rs.getString("timeFound"), rs.getString("passengerName"), rs.getString("passengerCity"), rs.getString("airportDeparture"), rs.getString("flightNumber"), rs.getString("typeLuggage"), rs.getString("brandLuggage"), rs.getString("color")));

        }
        return luggageData;
    }
}
